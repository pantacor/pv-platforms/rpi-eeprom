FROM alpine as qemu

RUN wget -O /qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-arm-static; \
        chmod a+x /qemu-arm-static


FROM arm32v6/alpine:3.12

ENV QEMU_ARCH=arm

COPY --from=qemu /qemu-arm-static /usr/bin/

RUN apk add raspberrypi \
	dash \
	pciutils

COPY rpi-eeprom-* /opt/rpi-eeprom/bin/
COPY run-rpi-eeprom-update /
COPY firmware /opt/rpi-eeprom/share/firmware
COPY rpi-eeprom-update-default /etc/default/rpi-eeprom-update

ENV PATH=$PATH:/opt/rpi-eeprom/bin/:/opt/vc/bin/

CMD /run-rpi-eeprom-update

